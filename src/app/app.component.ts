import {Component} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';
import {AngularFireAuth} from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import {Observable} from 'rxjs/Observable';
import {Router} from '@angular/router';
import {AuthService} from './service/auth.service';
import {BonPlanService} from './service/bon-plan.service';
import {BonPlanModel} from './model/bon-plan-model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  courses: any[];
  isLogged: Boolean;
  pseudo: String;
  email: String;
  user: Observable<firebase.User>;
  // map
  title = 'app';
  lat: 51.678418;
  lng: 7.809007;

  public bonPlans: Observable<BonPlanModel[]>;

  constructor(db: AngularFireDatabase, public authService: AuthService, public afAuth: AngularFireAuth, private router: Router,
              private bonPlanService: BonPlanService) {
    this.bonPlans = this.bonPlanService.getBonPlansList();

    this.user = this.authService.afAuth.authState;
    this.user.subscribe((auth) => {
      if (auth) {
        this.isLogged = true;
        this.pseudo = auth.displayName;
        this.email = auth.email;
        console.log('Connecté');
        console.log(auth);
        this.router.navigate(['']);
      } else {
        console.log('Deconnecte');
        this.isLogged = false;
        this.pseudo = '';
        this.email = '';
        // this.router.navigate(['login']);
      }
    });
  }
}
