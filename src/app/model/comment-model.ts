
export class CommentModel {
  $key: string;
  userid: string;
  username: string;
  useremail: string;
  comment: string;
  date: number

  constructor( userid: string, username: string, useremail: string, comment: string) {
    this.userid = userid;
    this.username = username;
    this.useremail = useremail;
    this.comment = comment;
    this.date = Date.now();
  }
}

