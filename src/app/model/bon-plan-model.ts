export class BonPlanModel {
  $key: string;
  idUser: string;
  name: string;
  tagline: string;
  description: string;
  category: string;
  coverUrl: string;
  address: string;
  latitude: number;
  longitude: number;
  telephone: number;
  cover2: string;


  constructor( idUser: string,
               name: string,
               tagline: string,
               description:  string,
               category: string,
               address: string,
               latitude: number,
               longitude: number,
               telephone: number,
               coverUrl: string,
               cover2: string) {
    this.idUser = idUser;
    this.name = name;
    this.tagline = tagline;
    this.description = description;
    this.category = category;
    this.address = address;
    this.latitude = latitude;
    this.longitude = longitude;
    this.telephone = telephone;
    this.coverUrl = coverUrl;
    this.cover2 = cover2;
  }
}
