import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../../service/auth.service';
import {UserModel} from '../../../model/user-model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user = new UserModel('', '', '');
  password = '';
  confirmPassword = '';
  errorMessage = '';

  constructor(public authService: AuthService, private  router: Router) {
  }

  ngOnInit() {
  }

  clearErrorMessage() {
    this.errorMessage = '';
  }

  onSignUp(): void {
    this.clearErrorMessage();

    if (this.validateForm(this.user.email, this.password, this.confirmPassword, this.user.firstName, this.user.lastName)) {
      this.authService.signUpWithEmail(this.user.email, this.password)
        .then((uid) => {
          this.authService.createUserInDB(uid, this.user);
          this.router.navigate(['/']);
        }).catch(_error => {
          this.errorMessage = _error.toString();
        });
    }
  }

  validateForm(email: string, password: string, confirmPassword: string, firstName: string, lastName: string): boolean {
    if (firstName.length === 0) {
      this.errorMessage = 'Please enter FirstName!';
      return false;
    }

    if (lastName.length === 0) {
      this.errorMessage = 'Please enter LastName!';
      return false;
    }

    if (email.length === 0) {
      this.errorMessage = 'Please enter Email!';
      return false;
    }

    if (!this.isValidMailFormat(email)) {
      this.errorMessage = 'Invalid Email!';
      return false;
    }

    if (password.length === 0) {
      this.errorMessage = 'Please enter Password!';
      return false;
    }

    if (password !== confirmPassword) {
      this.errorMessage = 'Password not confirmed!';
      return false;
    }

    this.errorMessage = '';

    return true;
  }

  isValidMailFormat(email: string) {
    const EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    if ((email.length === 0) || (!EMAIL_REGEXP.test(email))) {
      return false;
    }

    return true;
  }
}
