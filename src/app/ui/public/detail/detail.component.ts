import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {BonPlanService} from '../../../service/bon-plan.service';
import {BonPlanModel} from '../../../model/bon-plan-model';
import {CommentService} from '../../../service/comment.service';
import {CommentModel} from '../../../model/comment-model';
import {ToastrService} from 'ngx-toastr';
import {AuthService} from '../../../service/auth.service';
import {Observable} from 'rxjs/Observable';
import {FacebookService, InitParams, UIParams, UIResponse} from 'ngx-facebook';

declare var $;

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  bonplanKey: string;
  bonPlan: BonPlanModel;
  newComment = new CommentModel('', '', '', '');
  comments: Observable<CommentModel[]>;
  isLogged: Boolean = false;

  constructor(private route: ActivatedRoute, private bonplanService: BonPlanService,
              private commentsService: CommentService, private authService: AuthService, private toastr: ToastrService,
              private fb: FacebookService) {
    const initParams: InitParams = {
      appId: '177781389593931',
      xfbml: true,
      version: 'v2.8'
    };
    fb.init(initParams);

    this.authService.afAuth.authState.subscribe((auth) => {
      if (auth) {
        this.isLogged = true;
      } else {
        this.isLogged = false;
      }
    });
  }

  ngOnInit() {
    function hideShow() {
      const x = document.getElementById('toggle');
      if (x.style.display === 'none') {
        x.style.display = 'block';
      } else {
        x.style.display = 'none';
      }
    }

    $('#stars li').on('mouseover', function () {
      const onStar = parseInt($(this).data('value'), 10);
      $(this).parent().children('li.star').each(function (e) {
        if (e < onStar) {
          $(this).addClass('hover');
        } else {
          $(this).removeClass('hover');
        }
      });

    }).on('mouseout', function () {
      $(this).parent().children('li.star').each(function (e) {
        $(this).removeClass('hover');
      });
    });


    $('#stars li').on('click', function () {
      const onStar = parseInt($(this).data('value'), 10);
      const stars = $(this).parent().children('li.star');

      for (let i = 0; i < stars.length; i++) {
        $(stars[i]).removeClass('selected');
      }

      for (let i = 0; i < onStar; i++) {
        $(stars[i]).addClass('selected');
      }

      // JUST RESPONSE (Not needed)
      const ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
      let msg = '';
      if (ratingValue > 1) {
        msg = 'Thanks! You rated this ' + ratingValue + ' stars.';
      } else {
        msg = 'We will improve ourselves. You rated this ' + ratingValue + ' stars.';
      }
      responseMessage(msg);
    });

    function responseMessage(msg) {
      $('.success-box').fadeIn(200);
      $('.success-box div.text-message').html('<span>' + msg + '</span>');
    }

    this.route.params
      .switchMap((params: Params) => {
        this.bonplanKey = params['key'];
        return this.bonplanService.getBonPlan(this.bonplanKey);
      })
      .subscribe((bonPlan) => {
        this.bonPlan = bonPlan;
        this.bonPlan.$key = this.bonplanKey;
      });

    this.comments = this.commentsService.getCommentsList(this.bonplanKey); // Load data from firebase db
    this.comments.subscribe();
  }

  submitReview(): void {
    this.newComment.userid = this.authService.currentUserId;
    if (this.commentsService.addComment(this.newComment, this.bonPlan.$key, null, null)) {
      this.toastr.success('success ', 'Thanks for adding a review!');
      this.resetNewComment();
    } else {
      this.toastr.error('Please fill in the form', 'Error!');
    }
  }

  resetNewComment(): void {
    this.newComment = new CommentModel('', '', '', '');
  }

  facebookShareBonPlan(): void {

    const params: UIParams = {
      href: window.location.href,
      method: 'share'
    };

    this.fb.ui(params)
      .then((res: UIResponse) => console.log(res))
      .catch((e: any) => console.error(e));
  }
}
