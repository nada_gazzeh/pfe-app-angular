import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {GeolocationService} from '../../../service/geolocation.service';
import {MapsAPILoader} from '@agm/core';
import {FormControl} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {

  category: string;
  bonplanName: string;
  latitude: number;
  longitude: number;
  address: string;

  public autocompleteLocationControl: FormControl;

  @ViewChild('autocompleteLocation')
  public autocompleteElementRef: ElementRef;

  constructor(private geolocationService: GeolocationService, private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone, private router: Router) {
  }

  ngOnInit() {
    // create search FormControl
    this.autocompleteLocationControl = new FormControl();

    // load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.autocompleteElementRef.nativeElement, {
        types: ['geocode']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          // get the place result
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();

          // verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          // set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.address = place.formatted_address;
        });
      });
    });
  }

  showSearchResultScreen() {
    this.router.navigate(['listing'],
      {
        queryParams: {
          category: this.category, name: this.bonplanName, address: this.address,
          latitude: this.latitude, longitude: this.longitude
        }
      });
  }

  selectCategoryFood() {
    if (this.category !== 'Food') {
      this.category = 'Food';
    } else {
      this.category = null;
    }
  }

  selectCategoryCoffee() {
    if (this.category !== 'Coffee') {
      this.category = 'Coffee';
    } else {
      this.category = null;
    }
  }

  selectCategoryEvent() {
    if (this.category !== 'Event') {
      this.category = 'Event';
    } else {
      this.category = null;
    }
  }

  selectCategoryLeisure() {
    if (this.category !== 'Leisure') {
      this.category = 'Leisure';
    } else {
      this.category = null;
    }
  }

  selectCategoryShopping() {
    if (this.category !== 'Shopping') {
      this.category = 'Shopping';
    } else {
      this.category = null;
    }
  }
}
