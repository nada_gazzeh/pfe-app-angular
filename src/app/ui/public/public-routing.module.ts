import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {DetailComponent} from './detail/detail.component';
import {NotfoundComponent} from './notfound/notfound.component';
import {AddListingComponent} from './add-listing/add-listing.component';
import {ContactUsComponent} from './contact-us/contact-us.component';
import {InscriptionComponent} from './inscription/inscription.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {ForgotComponent} from './forgot/forgot.component';
import {ListingComponent} from './listing/listing.component';
import {EditProfilComponent} from './edit-profil/edit-profil.component';

const routes: Routes = [
  {
    path: '',
    component: IndexComponent,
  },
  {
    path: 'addlisting',
    component: AddListingComponent,
  },

  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'contact',
    component: ContactUsComponent,
    // loadChildren: './ui/client/client.module#ClientModule'
  },
  {
    path: 'detail/:key',
    component: DetailComponent,
    // loadChildren: './ui/client/client.module#ClientModule'
  },
  {
    path: 'inscription',
    component: InscriptionComponent,
    // loadChildren: './ui/client/client.module#ClientModule'
  },
  {
    path: 'register',
    component: RegisterComponent,
    // loadChildren: './ui/client/client.module#ClientModule'
  },
  {
    path: 'forgot',
    component: ForgotComponent,
    // loadChildren: './ui/client/client.module#ClientModule'
  },
  {
    path: 'listing',
    component: ListingComponent,
  },
{
  path: 'editProfil',
  component: EditProfilComponent,
},
  {
    path: '**',
    component: NotfoundComponent,
  }
  // {path: '', redirectTo: 'login', pathMatch: 'full'},
  // {path: 'login', component: UserLoginComponent},
  // {path: 'user', component: UserInfoComponent}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
