import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SliderComponent} from './slider/slider.component';
import {JumbotronComponent} from './jumbotron/jumbotron.component';
import {CarouselComponent} from './carousel/carousel.component';
import {FeedbackComponent} from './feedback/feedback.component';
import {IndexComponent} from './index/index.component';
import {DetailComponent} from './detail/detail.component';
import {SliderDetailComponent} from './slider-detail/slider-detail.component';
import {CarouselDetailComponent} from './carousel-detail/carousel-detail.component';
import {NotfoundComponent} from './notfound/notfound.component';
import {PublicRoutingModule} from './public-routing.module';
import {HomeComponent} from './home/home.component';
import {ContactUsComponent} from './contact-us/contact-us.component';
import {AddListingComponent} from './add-listing/add-listing.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {InscriptionComponent} from './inscription/inscription.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {ForgotComponent} from './forgot/forgot.component';
import {AngularFireModule} from 'angularfire2';
import {environment} from '../../../environments/environment';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AuthService} from '../../service/auth.service';
import {AgmCoreModule} from '@agm/core';
import {MatAutocompleteModule, MatInputModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PublicComponent} from './public.component';
import {GeolocationService} from '../../service/geolocation.service';
import {ListingComponent} from './listing/listing.component';
import { EditProfilComponent } from './edit-profil/edit-profil.component';

@NgModule({
  imports: [
    CommonModule,
    PublicRoutingModule,
    FormsModule,
    MatInputModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCm-OTEX2McFGLluJ-Lx_nkfgx4JCJmaTM',
      libraries: ['places']
    })
  ],
  providers: [AuthService, GeolocationService],
  declarations: [
    SliderComponent,
    JumbotronComponent,
    CarouselComponent,
    FeedbackComponent,
    IndexComponent,
    DetailComponent,
    SliderDetailComponent,
    CarouselDetailComponent,
    NotfoundComponent,
    HomeComponent,
    ContactUsComponent,
    AddListingComponent,
    InscriptionComponent,
    LoginComponent,
    RegisterComponent,
    ForgotComponent,
    ListingComponent,
    EditProfilComponent
  ]
})
export class PublicModule { }
