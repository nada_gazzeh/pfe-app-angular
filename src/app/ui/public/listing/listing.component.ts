import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BonPlanService} from '../../../service/bon-plan.service';
import {Observable} from 'rxjs/Observable';
import {BonPlanModel} from '../../../model/bon-plan-model';
import {$} from 'protractor';
import {GeolocationService} from '../../../service/geolocation.service';

@Component({
  selector: 'app-listing',
  templateUrl: './listing.component.html',
  styleUrls: ['./listing.component.css']
})
export class ListingComponent implements OnInit {
  category: string;
  bonplanName: string;
  address: string;
  latitude: number;
  longitude: number;
  bonPlans: Observable<BonPlanModel[]>;

  constructor(private route: ActivatedRoute, private router: Router, private bonPlanService: BonPlanService,
              private geolocationService: GeolocationService) {
  }

  ngOnInit() {
    this.route
      .queryParams
      .subscribe(params => {
        this.category = params['category'] || null;
        this.bonplanName = params['name'] || null;
        this.address = params['address'] || null;
        this.latitude = params['latitude'] || -1;
        this.longitude = params['longitude'] || -1;

        this.bonPlans = this.bonPlanService.getBonPlanByCategory(this.category);
        if (this.address != null) {
          this.bonPlans = this.bonPlans.map(arr => {
            const r: BonPlanModel[] = [];
            for (const entry of arr) {
              if (entry.address.toLowerCase().indexOf(this.address.toLowerCase()) >= 0) {
                r.push(entry);
              }
            }
            return r;
          });
        }
        if (this.bonplanName != null) {
          this.bonPlans = this.bonPlans.map(arr => {
            const r: BonPlanModel[] = [];
            for (const entry of arr) {
              if (entry.name.toLowerCase().indexOf(this.bonplanName.toLowerCase()) >= 0) {
                r.push(entry);
              }
            }
            return r;
          });
        }
      });
  }

}
