import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'app/service/auth.service';
import {IndexComponent} from '../index/index.component';

declare var $;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor( private authService: AuthService, private router: Router ) { }
  logout() {
    this.authService.logout();
    this.router.navigate(['IndexComponent']);
  }
  login() {
    this.authService.loginWithGoogle().then((data) => {
      this.router.navigate(['ConnectedComponent']);
    });
  }
  ngOnInit() {
}
}
