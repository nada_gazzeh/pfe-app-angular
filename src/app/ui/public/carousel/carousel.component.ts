import {Component, OnInit} from '@angular/core';
import {BonPlanModel} from '../../../model/bon-plan-model';
import {BonPlanService} from '../../../service/bon-plan.service';
import {Observable} from 'rxjs/Observable';
import {AngularFireList} from 'angularfire2/database';

declare var $;

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {
  bonPlans: Observable<BonPlanModel[]>;

  constructor(private bonPlanService: BonPlanService) {
  }

  ngOnInit() {
    this.bonPlans = this.bonPlanService.getBonPlansList(); // Load data from firebase db
    this.bonPlans.subscribe(re => {
      setTimeout(function () {
        $('#myCarousel').owlCarousel({
          loop: true,
          margin: 10,
          nav: true,
          navText: ['<i class="fas fa-chevron-circle-left"></i>\n' +
          '\n', '<i class="fas fa-chevron-circle-right"></i>\n' +
          '\n'],

          responsive: {
            0: {
              items: 1
            },
            600: {
              items: 3
            },
            1000: {
              items: 4
            }
          }
        });
      } , 1500);
    });
  }
}
