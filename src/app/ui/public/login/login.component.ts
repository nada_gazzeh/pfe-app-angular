import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../service/auth.service';
import {Router} from '@angular/router';
import {UserModel} from '../../../model/user-model';
import {FacebookService, LoginResponse} from 'ngx-facebook';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email = '';
  password = '';
  errorMessage = '';

  constructor(public authService: AuthService, private  router: Router,
              private fb: FacebookService) {
  //   loginWithFacebook(): void {
  //     this.fb.login()
  //     .then((response: LoginResponse) => console.log(response))
  //     .catch((error: any) => console.error(error));
  //
  // }
  }

  ngOnInit() {
  }

  clearErrorMessage() {
    this.errorMessage = '';
  }

  onLoginGoogle(): void {
    this.clearErrorMessage();
    this.authService.loginWithGoogle()
      .then((currentUser) => {
        const user = new UserModel(currentUser.email, currentUser.displayName, '');
        this.authService.createUserInDB(currentUser.uid, user);
        this.router.navigate(['/']);
      }).catch(_error => {
        this.errorMessage = _error.toString();
      });
  }

  onLoginEmail(): void {
    this.clearErrorMessage();

    if (this.validateForm(this.email, this.password)) {
      this.authService.loginWithEmail(this.email, this.password)
        .then(() => this.router.navigate(['/']))
        .catch(_error => {
          this.errorMessage = _error;
        });
    }
  }

  validateForm(email: string, password: string): boolean {
    if (email.length === 0) {
      this.errorMessage = 'Please enter Email!';
      return false;
    }

    if (!this.isValidMailFormat(email)) {
      this.errorMessage = 'Invalid Email!';
      return false;
    }

    if (password.length === 0) {
      this.errorMessage = 'Please enter Password!';
      return false;
    }

    if (password.length < 6) {
      this.errorMessage = 'Password should be at least 6 characters!';
      return false;
    }

    this.errorMessage = '';

    return true;
  }

  isValidMailFormat(email: string) {
    const EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    if ((email.length === 0) || (!EMAIL_REGEXP.test(email))) {
      return false;
    }

    return true;
  }
}
