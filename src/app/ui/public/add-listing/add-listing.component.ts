import {Component, OnInit} from '@angular/core';
import {BonPlanService} from '../../../service/bon-plan.service';
import {BonPlanModel} from '../../../model/bon-plan-model';
import {FileUpload} from '../../../model/file-upload';
import {AuthService} from '../../../service/auth.service';
import {ToastrModule, ToastrService} from 'ngx-toastr';
import {} from '@types/googlemaps';
import {GeolocationService} from '../../../service/geolocation.service';

declare var $;

@Component({
  selector: 'app-add-listing',
  templateUrl: './add-listing.component.html',
  styleUrls: ['./add-listing.component.css']
})

export class AddListingComponent implements OnInit {
  model = new BonPlanModel('', '', '', '', '', '', -1, -1, 0, '', '');
  imageFileUpload = new FileUpload(null);
  coverFileUpload = new FileUpload(null);

  progress: { percentage: number } = {percentage: 0};

  // Sousse location
  lat = 35.801757;
  lng = 10.614373;

  constructor(private authService: AuthService, private bonPlanService: BonPlanService, private toastr: ToastrService,
              private geolocationService: GeolocationService) {
  }

  ngOnInit() {
  }

  onSubmit() {
  }

  selectFileImage(event) {
    if (event.target.files[0].type.match('image.*')) {
      this.imageFileUpload = new FileUpload(event.target.files[0]);
    } else {
      alert('invalid image format!');
    }
  }

  selectFileCover(event) {
    if (event.target.files[0].type.match('image.*')) {
      this.coverFileUpload = new FileUpload(event.target.files[0]);
    } else {
      alert('invalid cover format!');
    }
  }

  saveBonPlan() {
    this.model.idUser = this.authService.currentUserId;
    if (this.bonPlanService.createBonPlan(this.model, this.imageFileUpload, this.coverFileUpload, this.progress)) {
      this.toastr.success('success ', 'Thanks for adding BonPlan!');
    } else {
      this.toastr.error('Please fill in the form', 'Error!');
    }

  }

  resetBonPlan() {
    this.model = new BonPlanModel(this.authService.currentUserId, '', '', '', '', '', -1, -1, 0, '', '');
    this.imageFileUpload = new FileUpload(null);
    this.coverFileUpload = new FileUpload(null);
  }

  // TODO: Remove this when we're done
  get diagnostic() {
    return JSON.stringify(this.model) + JSON.stringify(this.progress);
  }

  markerDragEnd(event) {
    console.log('lat:' + event.coords.lat + ', lng:' + event.coords.lng);

    this.geolocationService.geocode(event.coords.lat, event.coords.lng).subscribe(result => {
      this.model.latitude = event.coords.lat;
      this.model.longitude = event.coords.lng;
      this.model.address = result[0].formatted_address;
    }, error => {
      this.model.latitude = -1;
      this.model.longitude = -1;
      this.model.address = '';
    }, () => {
    });
  }
}
