import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../../service/auth.service';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotComponent implements OnInit {
  email = '';
  errorMessage = '';

  constructor(public authService: AuthService, private  router: Router) {
  }

  ngOnInit() {
  }

  resetPassword() {
    if (this.isValidMailFormat(this.email)) {
      this.authService.resetPassword(this.email);
      this.router.navigateByUrl('');
    } else {
      this.errorMessage = 'Invalid email !';
    }
  }

  isValidMailFormat(email: string) {
    const EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    if ((email.length === 0) || (!EMAIL_REGEXP.test(email))) {
      return false;
    }

    return true;
  }
}
