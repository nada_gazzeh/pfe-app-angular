import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../service/auth.service';


declare var $;
@Component({
  selector: 'app-header-connected',
  templateUrl: './header-connected.component.html',
  styleUrls: ['./header-connected.component.css']
})

export class HeaderConnectedComponent implements OnInit {
  constructor(public authService: AuthService) { }

  ngOnInit() {
    $('li').hover(function() {
      $(this).children('ul').stop().slideToggle(400);
    });
    $('.reveal').on('click', function () {
      const $pwd = $('.pwd');
      if ($pwd.attr('type') === 'password') {
        $pwd.attr('type', 'text');
      } else {
        $pwd.attr('type', 'password');
      }
    });
  }

  logout() {
    this.authService.logout();
  }

}
