import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { FacebookModule } from 'ngx-facebook';

const routes: Routes = [
  {
    path: '',
    loadChildren: './ui/public/public.module#PublicModule'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    FacebookModule.forRoot()
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
