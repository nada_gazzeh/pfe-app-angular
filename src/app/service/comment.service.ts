import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {AngularFireDatabase, AngularFireList, AngularFireObject} from 'angularfire2/database';
import {CommentModel} from '../model/comment-model';
import {BonPlanModel} from '../model/bon-plan-model';
import {FileUpload} from '../model/file-upload';

@Injectable()
export class CommentService {
  private basePath = '/comments';

  constructor(private db: AngularFireDatabase) {
  }

  getCommentsList(idBonPlan: String): Observable<CommentModel[]> {
    const commentsRef = this.db.list(`${this.basePath}/${idBonPlan}`);
    return commentsRef.snapshotChanges().map((arr) => {
      return arr.map((snap) => Object.assign(snap.payload.val(), { $key: snap.key }) );
    });
  }

  addComment(comment: CommentModel, idBonPlan: String, imageFileUpload: FileUpload, videoFileUpload: FileUpload): void {
    const commentsRef = this.db.database.ref(`${this.basePath}/${idBonPlan}`);
    commentsRef.push(comment);
  }
}
