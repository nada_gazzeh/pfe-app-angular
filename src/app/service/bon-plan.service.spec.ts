import { TestBed, inject } from '@angular/core/testing';

import { BonPlanService } from './bon-plan.service';

describe('BonPlanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BonPlanService]
    });
  });

  it('should be created', inject([BonPlanService], (service: BonPlanService) => {
    expect(service).toBeTruthy();
  }));
});
