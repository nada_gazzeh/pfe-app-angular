import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase';
import {AngularFireDatabase, AngularFireList} from 'angularfire2/database';
import {BonPlanModel} from '../model/bon-plan-model';
import {UserModel} from '../model/user-model';

@Injectable()
export class AuthService {

  private usersBasePath = '/users';
  authState: any = null;
  provider: firebase.auth.GoogleAuthProvider;

  constructor(public afAuth: AngularFireAuth, private db: AngularFireDatabase) {
     this.provider = new firebase.auth.GoogleAuthProvider();
    this.afAuth.authState.subscribe((auth) => {
      this.authState = auth;
    });
  }

  get isUserAnonymousLoggedIn(): boolean {
    return (this.authState !== null) ? this.authState.isAnonymous : false;
  }

  get currentUserId(): string {
    return (this.authState !== null) ? this.authState.uid : '';
  }

  get currentUserName(): string {
    return this.authState['email'];
  }

  get currentUser(): any {
    return (this.authState !== null) ? this.authState : null;
  }

  get isUserEmailLoggedIn(): boolean {
    if ((this.authState !== null) && (!this.isUserAnonymousLoggedIn)) {
      return true;
    } else {
      return false;
    }
  }

  signUpWithEmail(email: string, password: string) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then((user) => {
        this.authState = user;
        return firebase.auth().currentUser.uid;
      })
      .catch(error => {
        console.log(error);
        throw error;
      });
  }

  loginWithEmail(email: string, password: string) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then((user) => {
        this.authState = user;
      })
      .catch(error => {
        console.log(error);
        throw error;
      });
  }

  resetPassword(email: string) {
    return this.afAuth.auth.sendPasswordResetEmail(email)
      .then(() => console.log('sent Password Reset Email!'))
      .catch((error) => console.log(error));
  }
  loginWithGoogle() {
    return this.afAuth.auth.signInWithPopup(this.provider)
      .then((user) => {
        this.authState = user;
        return firebase.auth().currentUser;
      })
      .catch(error => {
        console.log(error);
        throw error;
      });
  }
  logout() {
    return this.afAuth.auth.signOut();
  }

  createUserInDB(uid: string, user: UserModel) {
    const usersRef = this.db.database.ref(`${this.usersBasePath}/${uid}`);
    usersRef.set(user);
  }
}
