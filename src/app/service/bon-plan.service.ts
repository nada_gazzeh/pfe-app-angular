import {Injectable} from '@angular/core';
import {AngularFireDatabase, AngularFireList} from 'angularfire2/database';
import {Observable} from 'rxjs/Observable';
import {BonPlanModel} from '../model/bon-plan-model';
import * as firebase from 'firebase';
import {FileUpload} from '../model/file-upload';

@Injectable()
export class BonPlanService {

  private basePath = '/bonPlans';
  private storageBasePath = '/uploads';

  plansRef: AngularFireList<BonPlanModel>;

  constructor(private db: AngularFireDatabase) {
    this.plansRef = db.list(`${this.basePath}`);
  }

  getBonPlansList(): Observable<BonPlanModel[]> {
    return this.plansRef.snapshotChanges().map((arr) => {
      return arr.map((snap) => Object.assign(snap.payload.val(), {$key: snap.key}));
    });
  }

  getBonPlan(key: string): Observable<BonPlanModel | null> {
    const planPath = `${this.basePath}/${key}`;
    const plan = this.db.object(planPath).valueChanges() as Observable<BonPlanModel | null>;
    return plan;
  }

  getBonPlanByCategory(category: string): Observable<BonPlanModel[] | null> {
    return this.plansRef.snapshotChanges().map(arr => {
      const result: BonPlanModel[] = [];
      for (const entry of arr) {
        const bonPlan: BonPlanModel = entry.payload.val();
        if (category === null || bonPlan.category.toLowerCase() === category.toLowerCase()) {
          bonPlan.$key = entry.key;
          result.push(bonPlan);
        }
      }
      return result;
    });
  }

  getBonPlanByAddress(address: string): Observable<BonPlanModel[] | null> {
    return this.plansRef.snapshotChanges().map(arr => {
      const result: BonPlanModel[] = [];
      for (const entry of arr) {
        const bonPlan: BonPlanModel = entry.payload.val();
        if (bonPlan.address.toLowerCase().indexOf(address.toLowerCase() + ',') >= 0) {
          bonPlan.$key = entry.key;
          result.push(bonPlan);
        }
      }
      return result;
    });
  }

  getBonPlanByName(name: string): Observable<BonPlanModel[] | null> {
    return this.plansRef.snapshotChanges().map(arr => {
      const result: BonPlanModel[] = [];
      for (const entry of arr) {
        const bonPlan: BonPlanModel = entry.payload.val();
        if (bonPlan.name.toLowerCase().indexOf(name.toLowerCase()) >= 0) {
          bonPlan.$key = entry.key;
          result.push(bonPlan);
        }
      }
      return result;
    });
  }

  createBonPlan(plan: BonPlanModel, imageFileUpload: FileUpload, coverFileUpload: FileUpload, progress: { percentage: number }): void {

    console.log('--> 0');
    if (imageFileUpload.file !== null) {
      console.log('--> 1');
      this.uploadBonPlanImageFile(plan, imageFileUpload, progress);
    } else if (coverFileUpload.file !== null) {
      console.log('--> 2');
      this.uploadBonPlanCoverFile(plan, coverFileUpload, progress);
    } else {
      console.log('--> 3');
      this.plansRef.push(plan)
        .then((plans) => {
          console.log(plans);
          if (plans) {
            return true;
          } else {
            return false;
          }
        });
    }
  }

  uploadBonPlanImageFile(plan: BonPlanModel, imageFileUpload: FileUpload, progress: { percentage: number }): void {
    const storageRef = firebase.storage().ref();
    const uploadTask = storageRef.child(`${this.storageBasePath}/${imageFileUpload.file.name}`).put(imageFileUpload.file);

    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) => {
        // in progress
        const snap = snapshot as firebase.storage.UploadTaskSnapshot;
        progress.percentage = Math.round((snap.bytesTransferred / snap.totalBytes) * 100);
      },
      (error) => {
        // fail
        console.log(error);

        this.uploadBonPlanCoverFile(plan, imageFileUpload, progress);
      },
      () => {
        // success
        plan.coverUrl = uploadTask.snapshot.downloadURL;

        this.uploadBonPlanCoverFile(plan, imageFileUpload, progress);
      }
    );
  }

  uploadBonPlanCoverFile(plan: BonPlanModel, coverFileUpload: FileUpload, progress: { percentage: number }): void {
    const storageRef = firebase.storage().ref();
    const uploadTask = storageRef.child(`${this.storageBasePath}/${coverFileUpload.file.name}`).put(coverFileUpload.file);

    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) => {
        // in progress
        const snap = snapshot as firebase.storage.UploadTaskSnapshot;
        progress.percentage = Math.round((snap.bytesTransferred / snap.totalBytes) * 100);
      },
      (error) => {
        // fail
        console.log(error);
      },
      () => {
        // success
        plan.cover2 = uploadTask.snapshot.downloadURL;

        // Save element in db
        this.plansRef.push(plan);
      }
    );
  }

  updateBonPlan(key: string, plan: BonPlanModel): void {
    this.plansRef.update(key, plan);
  }

  deleteBonPlan(key: string): void {
    this.plansRef.remove(key);
  }

  deleteAll(): void {
    this.plansRef.remove();
  }

  // Default error handling for all actions
  private handleError(error: Error) {
    console.error(error);
  }
}
