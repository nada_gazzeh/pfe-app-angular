import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {HeaderComponent} from './ui/Layout/header/header.component';
import {FooterComponent} from './ui/Layout/footer/footer.component';
import {PublicComponent} from './ui/public/public.component';
import {PublicModule} from './ui/public/public.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {environment} from '../environments/environment';
import {AngularFireAuth} from 'angularfire2/auth';
import {AuthService} from './service/auth.service';
import {AppRoutingModule} from './app-routing.module';
import {BonPlanService} from './service/bon-plan.service';
import {HeaderConnectedComponent} from './ui/Layout/header-connected/header-connected.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import {FacebookModule, FacebookService} from 'ngx-facebook';
import {CommentService} from './service/comment.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    PublicComponent,
    HeaderConnectedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PublicModule,
    NgbModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    }),
    FacebookModule.forRoot()
  ],
  providers: [AuthService,
    AngularFireAuth,
    BonPlanService,
    CommentService,
    FacebookService],
  bootstrap: [AppComponent]
})

export class AppModule {
}

// export function MapServiceProviderFactory() {
//   const bc: BingMapAPILoaderConfig = new BingMapAPILoaderConfig();
//   bc.apiKey = 'AIzaSyCm-OTEX2McFGLluJ-Lx_nkfgx4JCJmaTM'; // your bing map key
//   bc.branch = 'experimental';
//   return new BingMapAPILoader(bc, new WindowRef(), new DocumentRef());
// }
