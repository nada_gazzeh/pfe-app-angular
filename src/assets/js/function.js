$(document).ready(function () {
    $('body').append('<div id="toTop" class="btn btn-info"><span class="glyphicon glyphicon-chevron-up"></span> Back to Top</div>');
    $(window).scroll(function () {
        if ($(this).scrollTop() !== 0) {
            $('#toTop').fadeIn();
        } else {
            $('#toTop').fadeOut();
        }
    });
    $('#toTop').click(function () {
        $("html, body").animate({scrollTop: 0}, 600);
        return false;
    });
});

$('.dropdown-toggle').dropdown();

/************* Google Maps *******************/
var map;

function initMap() {
    var myCenter = new google.maps.LatLng(35.820390,10.593090);

    map = new google.maps.Map(document.getElementById('map'), {
        center: myCenter,
        zoom: 12
    });

    addMarker(myCenter, 'Marker 1', 'HELLO WORLD');
    addMarker(new google.maps.LatLng(35.820390,10.553090), 'Marker 2', 'HELLO WORLD');
    addMarker({lat:35.820390,lng:10.623090}, 'Marker 3', 'HELLO WORLD');
}

function addMarker(location, title, contentString) {
    var marker = new google.maps.Marker({
        map: map,
        title:title,
        position:location,
        animation: google.maps.Animation.DROP
    });

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });

}
/********************************/
// Plugin options and our code
// $("#modal_trigger").leanModal({
//   top: 100,
//   overlay: 0.6,
//   closeButton: ".modal_close"
// });

$(function() {
  // Calling Login Form
  $("#login_form").click(function() {
    $(".social_login").hide();
    $(".user_login").show();
    return false;
  });

  // Calling Register Form
  $("#register_form").click(function() {
    $(".social_login").hide();
    $(".user_register").show();
    $(".header_title").text('Register');
    return false;
  });

  // Going back to Social Forms
  $(".back_btn").click(function() {
    $(".user_login").hide();
    $(".user_register").hide();
    $(".social_login").show();
    $(".header_title").text('Login');
    return false;
  });
});
