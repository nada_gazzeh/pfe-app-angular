// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
  apiKey: 'AIzaSyDXBPU8ooXXNrinIrJLlN77VTMCKWAfKkM',
  authDomain: 'bon-plan-pfe2018.firebaseapp.com',
  databaseURL: 'https://bon-plan-pfe2018.firebaseio.com',
  projectId: 'bon-plan-pfe2018',
  storageBucket: 'bon-plan-pfe2018.appspot.com',
  messagingSenderId: '922858401133'
  }
};
